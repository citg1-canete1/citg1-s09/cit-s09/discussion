package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SpringBootApplication
//This application will function as an
// endpoint that will used in handling http request.
@RestController
//// will require all routes
@RequestMapping("/greeting")
public class DiscussionApplication {
	//private List<Student> students = new ArrayList<>();
	public static ArrayList<String> enrollees = new ArrayList<String>();


	public static void main(String[] args) {SpringApplication
				.run(DiscussionApplication.class, args);}

	/*private static class Student {
		private final int id;
		private final String name;
		private final String course;

		public Student(int id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getCourse() {
			return course;
		}
	}*/

	//localhost:8080/hello
	/*@GetMapping("/hello")

	//Maps a get request route "/hello" and
	// invoked the method hello().
	public String hello(){
		return "Hello World";
	}
	//localhost:8080/hi?name=value
	// Route with String Query
	@GetMapping("/hi")
	//@RequestParam annotation that will allow us to extract data from the url
	public String hi(@RequestParam(value="name",
			defaultValue = "John") String name){
		return String.format("Hi %s", name);

	}

	//Multiple parameters
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name",
	defaultValue = "Joe") String name, @RequestParam(value="friend",
	defaultValue ="Jane")String friend){
		return String.format("Hello %s! My name is %s.", friend,name);
	}

	//Route with path variables
	//Dynamic data is obtained directly from the url
	//localhost:8080/name
	@GetMapping("/hello/{name}")
	//@PathVariable annotation allows us ti extract data directly from url
	public String greetFriend(@PathVariable("name")String name){
		return String.format("Nice to meet you %s!", name);

	}
	//http://localhost:8080/greeting/welcome?user=value&role=value
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user") String user, @RequestParam(value="role") String role) {
		String message;

		switch (role) {
			case "admin":
				message = ("Welcome back to the class portal, Admin " + user + "!");
				break;
			case "teacher":
				message = ("Thank you for logging in, Teacher " + user + "!");
				break;
			case "student":
				message = ("Welcome to the class, " + user + "!");
				break;
			default:
				message = ("Role out of range!");
		}

		return message;
	}
	//http://localhost:8080/greeting/register?id=value&name=value&course=value
	@GetMapping("/register")
	public String register(@RequestParam(value="id") int id, @RequestParam(value="name") String name, @RequestParam(value="course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return id + " your id number is registered on the system!";
	}
	//http://localhost:8080/greeting/account/{id}
	@GetMapping("/account/{id}")
	public String account(@PathVariable(value="id") int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				return "Welcome back " + student.getName() + "! You are currently enrolled in " + student.getCourse();
			}
		}
		return "Your provided " + id + " is not found in the system!";
	}*/

	//http://localhost:8080/greeting/enroll?user=value
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user) {
		DiscussionApplication.enrollees.add(user);
		return "Welcome, " + user + "!";
	}
	//http://localhost:8080/greeting/getEnrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return DiscussionApplication.enrollees.toString();
	}


	//http://localhost:8080/greeting/nameage?name=value&age=value
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name") String name, @RequestParam(value = "age") int age) {
		return "Hello, " + name + "! My age is " + age + ".";
	}

	//http://localhost:8080/greeting/course/{id}
	@GetMapping("/course/{id}")
	public String course(@PathVariable(value = "id") String id) {
		switch (id) {
			case "java101":
				return "Name: Java101,\nSchedule: MWF 8:00 AM - 11:00 AM,\nPrice: PHP 3000.00";
			case "sql101":
				return "Name: SQL101,\nSchedule: TTH 1:00 PM - 4:00 PM,\nPrice: PHP 2000.00";
			case "javaee101":
				return "Name: Java EE101,\nSchedule: MWF 1:00 PM - 4:00 AM,\nPrice: PHP 3500.00";
			default:
				return "Course not found.";
		}
	}
}
